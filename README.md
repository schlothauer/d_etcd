Simple image that contains etcd keystore


Version: 0.1


To start a container from that image call `./bin/start.sh`

Basic commands to play with etcd
================================

```bash
# query version from etcd
curl -L http://MY_LOCAL_IP:2379/version

# set message to 'Hello world'
curl http://127.0.0.1:2379/v2/keys/message -XPUT -d value="Hello world"

# read 'message' key 
curl http://127.0.0.1:2379/v2/keys/message
```

For API description see here [https://coreos.com/etcd/docs/latest/v2/api.html]




